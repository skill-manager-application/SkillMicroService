package com.soprahr.SkillService.entites;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "userInfo")
public class UserInfo {
	
	@Id
	private String id;
	private String userId;
	private List<skills> skills;
	private float skillLevel;
	private float interstLevel;
	private float competencyLevel;
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public List<skills> getSkills() {
		return skills;
	}


	public void setSkills(List<skills> skills) {
		this.skills = skills;
	}


	public float getCompetencyLevel() {
		return competencyLevel;
	}


	public void setCompetencyLevel(float competencyLevel) {
		this.competencyLevel = competencyLevel;
	}


	public static class skills{
		private String skillId;
		private String skillName;
	    private float selfAssignment;
	    private float supervisorAssignment;
	    private float intersetLevel;
	    private float avgLevel;
	    private int scroce;
	    private boolean testToken;
	    private LocalDateTime passedAt;
	    private LocalDateTime assigneddAt;
		public String getSkillId() {
			return skillId;
		}
		public void setSkillId(String skillId) {
			this.skillId = skillId;
		}
		public float getSelfAssignment() {
			return selfAssignment;
		}
		public void setSelfAssignment(float selfAssignment) {
			this.selfAssignment = selfAssignment;
		}
		public float getSupervisorAssignment() {
			return supervisorAssignment;
		}
		public void setSupervisorAssignment(float supervisorAssignment) {
			this.supervisorAssignment = supervisorAssignment;
		}
		public float getIntersetLevel() {
			return intersetLevel;
		}
		public void setIntersetLevel(float intersetLevel) {
			this.intersetLevel = intersetLevel;
		}
		public float getAvgLevel() {
			return avgLevel;
		}
		public void setAvgLevel(float avgLevel) {
			this.avgLevel = avgLevel;
		}
		public int getScroce() {
			return scroce;
		}
		public void setScroce(int scroce) {
			this.scroce = scroce;
		}
		public boolean isTestToken() {
			return testToken;
		}
		public void setTestToken(boolean testToken) {
			this.testToken = testToken;
		}
		public LocalDateTime getPassedAt() {
			return passedAt;
		}
		public void setPassedAt(LocalDateTime passedAt) {
			this.passedAt = passedAt;
		}
		public LocalDateTime getAssigneddAt() {
			return assigneddAt;
		}
		public void setAssigneddAt(LocalDateTime assigneddAt) {
			this.assigneddAt = assigneddAt;
		}
		public String getSkillName() {
			return skillName;
		}
		public void setSkillName(String skillName) {
			this.skillName = skillName;
		}
		
	}
	
	public void addSkills(skills skill) {
		this.skills.add(skill);
	}
	
	public void removeSkills(String skillId) {
		skills skill = this.skills.stream().filter(t -> t.getSkillId() == skillId).findFirst().orElse(null);
		if(skill != null) {
			this.skills.remove(skill);
		}
	}

	public float getSkillLevel() {
		return skillLevel;
	}


	public void setSkillLevel(float skillLevel) {
		this.skillLevel = skillLevel;
	}


	public float getInterstLevel() {
		return interstLevel;
	}


	public void setInterstLevel(float interstLevel) {
		this.interstLevel = interstLevel;
	}
}
