package com.soprahr.SkillService.entites;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "skills")
public class SkillEntity {


	@Id
    private String id;

    private String name;

    private String description;  
    
    //private String ppl assinged too
    //departmnt assingzed to
    //team //
    // role // 
     
    @DBRef
    private CategoryEntity category;
    
    //minimum levelbech naaml aka star ahmer fy diagrmme 
    private float level;
   
    private List<String> keywords;

    private List<String> relatedSkills;

    private List<String> resources;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    public SkillEntity(String name, String description, CategoryEntity category, float level, List<String> keywords,
                       List<String> relatedSkills, List<String> resources, LocalDateTime createdAt,
                       LocalDateTime updatedAt) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.level = level;
        this.keywords = keywords;
        this.relatedSkills = relatedSkills;
        this.resources = resources;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public List<String> getRelatedSkills() {
		return relatedSkills;
	}

	public void setRelatedSkills(List<String> relatedSkills) {
		this.relatedSkills = relatedSkills;
	}

	public List<String> getResources() {
		return resources;
	}

	public void setResources(List<String> resources) {
		this.resources = resources;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
}
