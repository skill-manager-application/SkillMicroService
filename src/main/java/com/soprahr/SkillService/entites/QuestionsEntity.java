package com.soprahr.SkillService.entites;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "questions")
public class QuestionsEntity {
	
	@Id
    private String id;
	
	private String questionText;
	private String explanation;
	private List<Options> options = new ArrayList<>();
	
	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}

	public String getQuestionText() {
		return questionText;
	}




	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}




	public String getExplanation() {
		return explanation;
	}




	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	public List<Options> getOptions() {
		return options;
	}




	public void setOptions(List<Options> options) {
		this.options = options;
	}








	public static class Options{
		private String text;
		private boolean correct;
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		public boolean isCorrect() {
			return correct;
		}
		public void setCorrect(boolean correct) {
			this.correct = correct;
		}
		
	}
}
