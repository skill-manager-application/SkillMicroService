package com.soprahr.SkillService.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.SkillService.Repository.userInfoRepository;
import com.soprahr.SkillService.Service.ServiceSkills;
import com.soprahr.SkillService.Service.ServiceUserInfo;
import com.soprahr.SkillService.entites.SkillEntity;
import com.soprahr.SkillService.entites.UserInfo;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {
	@Autowired
	ServiceUserInfo userInfoservice;
	//try make userid is the id of userinfo to get it
	@Autowired
	userInfoRepository userInfoRepository;
	
	
	@Autowired
	ServiceSkills skillService;
	
	
	//Method Add new Skill To database using SkillRequest and Category ID as parameters
			@PostMapping(consumes =MediaType.APPLICATION_JSON_VALUE,
						 produces =MediaType.APPLICATION_JSON_VALUE)
			public ResponseEntity<UserInfo> createSkill(@RequestBody UserInfo userInfo) throws Exception {
				//Save the Skill to Database
				UserInfo newSkill =  userInfoservice.createUserInfo(userInfo);
		        
		        //Map the SkillEntity to Skill Response
		    	
				return new ResponseEntity<UserInfo>(newSkill,HttpStatus.CREATED);
		    }
			
			//Method to get All Skills in database
		    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
		    public ResponseEntity<List<UserInfo>> getSkills(){
		    	//Get Skills from database and store it in listSkills
		    	List<UserInfo> listSkills = userInfoservice.getAllUserInfos();  
		    	
				
				return new ResponseEntity<List<UserInfo>>(listSkills,HttpStatus.OK);
		    }
			//Method to get All Skills in database
		    @GetMapping(path="/{userId}",produces = MediaType.APPLICATION_JSON_VALUE)
		    public Object getuserById(@PathVariable String userId){
		    	//Get Skills from database and store it in listSkills
		    	System.out.println(userId);
		    	//amml condition kenhom zouz imed obligatoire lahkeya plz
		    	UserInfo user =  userInfoRepository.findUserInfoByUserId(userId);  
		    	
				return user;
//				return new ResponseEntity<UserInfo>(listSkills,HttpStatus.OK);
		    }
		    @GetMapping("/skill/{skillId}")
		    public List<UserInfo> getUsersBySkill(@PathVariable String skillId) {
		        return userInfoRepository.findBySkillsSkillId(skillId);
		    }
		    // zyd nejbed hasb el category
		    
		   
		    
		    @GetMapping("/category/{userId}")
		    public HashMap<String, List<Float>>  getCatUserInfo(@PathVariable String userId) {
		    	HashMap<String, List<Float>> infoUser = new HashMap<>();
		    	UserInfo user =  userInfoRepository.findUserInfoByUserId(userId);  
		        for (int i = 0; i < user.getSkills().size(); i++) {
		        	SkillEntity skill = skillService.getSkillById(user.getSkills().get(i).getSkillId());
		        	//nicepage
		        	System.out.print(infoUser.get(skill.getCategory().getName()));
			       if(infoUser.size() > 0 && (infoUser.get(skill.getCategory().getName()) != null )){
			    	   List<Float> list = new ArrayList<>();
			    	   
			    	   list.add((infoUser.get(skill.getCategory().getName()).get(1) + skill.getLevel())/2);
			    	   list.add( (infoUser.get(skill.getCategory().getName()).get(0) + user.getSkills().get(i).getAvgLevel())/2);
			    	   infoUser.put(skill.getCategory().getName(),list);
			       }
			       else {
			    	   List<Float> list = new ArrayList<>();
			    	   list.add(skill.getLevel());
			    	   list.add( user.getSkills().get(i).getAvgLevel());
			    	   infoUser.put(skill.getCategory().getName(),list);
			       }
				}
		        return infoUser;
		    }
		    
		    
		    
		    @PostMapping(produces =MediaType.APPLICATION_JSON_VALUE)
		    public UserInfo addSkillToUser(@RequestParam String skillId,@RequestParam String userId) {
		    	return userInfoservice.addSkillToUser(skillId,userId);
		    }
		    
		    // zyd aamel fnct tjyblk ely aandhom skill heky todkhel lel base mena w tbarbech w trajaahom
		    // ky tasnaa new user taaml maa useinfo 
		  // el fonction ely lezem nkhdemha taw hiya tekhlo list String id users w tkhrjly menhom skills lkol heka win wsolt ena w fy angular thot fy create data fy dep info
		    
}
