package com.soprahr.SkillService.controllers;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.SkillService.Repository.SkillRepository;
import com.soprahr.SkillService.Request.SkillRequest;
import com.soprahr.SkillService.Response.SkillResponse;
import com.soprahr.SkillService.Service.ServiceSkills;
import com.soprahr.SkillService.entites.SkillEntity;
import com.soprahr.SkillService.entites.UserInfo;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/skills")
public class SkillController {
	
	@Autowired
	ServiceSkills skillService;
	
	@Autowired
	SkillRepository repository;

		//Method Add new Skill To database using SkillRequest and Category ID as parameters
		@PostMapping(consumes =MediaType.APPLICATION_JSON_VALUE,
					 produces =MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<SkillResponse> createSkill(@RequestBody SkillRequest skillRequest,@RequestParam String id) throws Exception {
			//Save the Skill to Database
	        SkillEntity newSkill =  skillService.createSkill(skillRequest,id);
	        
	        //Map the SkillEntity to Skill Response
	    	ModelMapper modelMapper = new ModelMapper();
	    	SkillResponse SkillResponse = modelMapper.map(newSkill, SkillResponse.class);
			return new ResponseEntity<SkillResponse>(SkillResponse,HttpStatus.CREATED);
	    }
	    
		//Method to get All Skills in database
	    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<List<SkillResponse>> getSkills(){
	    	//Get Skills from database and store it in listSkills
	    	List<SkillEntity> listSkills = skillService.getAllSkills();  
	    	List<SkillResponse> SkillsResponses = new  ArrayList<>();
	    	
	    	// Changes the Output form SkillEntity To SkillResponse
	    	ModelMapper modelMapper = new ModelMapper();
			for (SkillEntity skill : listSkills) {
				//Copy All properties From SkillEntity to SkillResponse
				SkillResponse SkillsResponse = modelMapper.map(skill, SkillResponse.class);
				SkillsResponse.setCategoryID(skill.getCategory().getId());
				SkillsResponse.setCategoryName(skill.getCategory().getName());
				//Add the current Skill To SkillResponses
				SkillsResponses.add(SkillsResponse);
			}
			
			return new ResponseEntity<List<SkillResponse>>(SkillsResponses,HttpStatus.OK);
	    }
	    
	    
	    // Delete Skill Based on it's id
		@DeleteMapping(path="/{id}")
		public ResponseEntity<Object> deleteUser(@PathVariable String id) {
			//Call SkillService to do the job
			skillService.deleteSkill(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
		 @GetMapping(path="/{skillId}")
		    public SkillResponse getUsersBySkill(@PathVariable String skillId) {
			 	SkillEntity skill =  repository.findById(skillId).get();
		        
		    	ModelMapper modelMapper = new ModelMapper();

		        SkillResponse SkillsResponse = modelMapper.map(skill, SkillResponse.class);
				SkillsResponse.setCategoryID(skill.getCategory().getId());
				SkillsResponse.setCategoryName(skill.getCategory().getName());
				return SkillsResponse;
		    }
		 
		 
		 
		 
		
}
