package com.soprahr.SkillService.controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.SkillService.Response.CategoryResponse;
import com.soprahr.SkillService.Service.ServiceCategory;
import com.soprahr.SkillService.entites.CategoryEntity;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/category")
public class CategoryController {
	@Autowired
	ServiceCategory serviceCategory;
	
	
		//Method Add new Category To database using only the name
		@PostMapping(consumes =MediaType.APPLICATION_JSON_VALUE,
					 produces =MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<CategoryEntity> createCategory(@RequestBody String name) throws Exception  {
			//Save the Category into database using CategoryService
			CategoryEntity newCategory =  serviceCategory.createCategory(name);
			return new ResponseEntity<CategoryEntity>(newCategory,HttpStatus.CREATED);
	    }
	    
		//Method to get All Categories in database
	    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<List<CategoryResponse>> getCategories() {
	    	//Get Categories from database and store it in listCategories
	    	List<CategoryEntity> CategoryList = serviceCategory.getAllCategorys();	
	    	
	    	// Changes the Output form CategoryEntity To CategoryResponse
	    	List<CategoryResponse> categoryResponses = new ArrayList<>();
	    	ModelMapper modelMapper = new ModelMapper();
			for (CategoryEntity Category : CategoryList) {
				//Copy All properties From CategoryEntity to CategoryResponse
				CategoryResponse categoryResponse = modelMapper.map(Category, CategoryResponse.class);
				//Add the current Category To CategoryResponses
				categoryResponses.add(categoryResponse);
			}
			return new ResponseEntity<List<CategoryResponse>>(categoryResponses,HttpStatus.OK);
	    } 
	    
	    // Delete Category Based on it's id
		@DeleteMapping(path="/{id}")
		public ResponseEntity<Object> deleteCategory(@PathVariable String id) {
			serviceCategory.deleteCategory(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	
	
}
