package com.soprahr.SkillService.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.SkillService.Repository.QuestionsRepository;
import com.soprahr.SkillService.Service.ServiceSkills;
import com.soprahr.SkillService.entites.QuestionsEntity;

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/questions")
public class QuestionController {
	@Autowired
	QuestionsRepository questionsRepository;
	@Autowired
	ServiceSkills skillService;
	
	//Method Add new Skill To database using SkillRequest and skill ID as parameters
			@PostMapping(consumes =MediaType.APPLICATION_JSON_VALUE,
						 produces =MediaType.APPLICATION_JSON_VALUE)
			public ResponseEntity<QuestionsEntity> createSkill(@RequestBody QuestionsEntity questionsEntity) throws Exception {
				//Save the Skill to Database
				
		        QuestionsEntity newQuestion = questionsRepository.save(questionsEntity);
		        //Map the SkillEntity to Skill Response
				return new ResponseEntity<QuestionsEntity>(newQuestion,HttpStatus.CREATED);
		    }
		    
			//Method to get All Skills in database
		    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
		    public ResponseEntity<List<QuestionsEntity>> getSkills(){
		    	//Get Skills from database and store it in listSkills
		    	List<QuestionsEntity> listSkills = questionsRepository.findAll();
		    	
		    	// Changes the Output form SkillEntity To SkillResponse
		    	
				
				return new ResponseEntity<List<QuestionsEntity>>(listSkills,HttpStatus.OK);
		    }
}
