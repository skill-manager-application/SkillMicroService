package com.soprahr.SkillService.Response;

import java.time.LocalDateTime;
import java.util.List;

public class SkillResponse {
	 private String id;

	    private String name;

	    private String description;
	    
	    private String categoryID;
	    
	    private String categoryName;

	    private float level;

	    private List<String> keywords;

	    private List<String> relatedSkills;

	    private List<String> resources;

	    private LocalDateTime createdAt;

	    private LocalDateTime updatedAt;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getCategoryID() {
			return categoryID;
		}

		public void setCategoryID(String categoryID) {
			this.categoryID = categoryID;
		}

		public String getCategoryName() {
			return categoryName;
		}

		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}

		public float getLevel() {
			return level;
		}

		public void setLevel(float level) {
			this.level = level;
		}

		public List<String> getKeywords() {
			return keywords;
		}

		public void setKeywords(List<String> keywords) {
			this.keywords = keywords;
		}

		public List<String> getRelatedSkills() {
			return relatedSkills;
		}

		public void setRelatedSkills(List<String> relatedSkills) {
			this.relatedSkills = relatedSkills;
		}

		public List<String> getResources() {
			return resources;
		}

		public void setResources(List<String> resources) {
			this.resources = resources;
		}

		public LocalDateTime getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(LocalDateTime createdAt) {
			this.createdAt = createdAt;
		}

		public LocalDateTime getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(LocalDateTime updatedAt) {
			this.updatedAt = updatedAt;
		}

}
