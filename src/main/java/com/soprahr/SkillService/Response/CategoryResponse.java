package com.soprahr.SkillService.Response;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class CategoryResponse {
	private String id;

	private String name;

	private List<SkillCategoryResponse> skills = new ArrayList<>();

	private LocalDateTime createdAt;

	private LocalDateTime updatedAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public List<SkillCategoryResponse> getSkills() {
		return skills;
	}

	public void setSkills(List<SkillCategoryResponse> skills) {
		this.skills = skills;
	}

	public static class SkillCategoryResponse{
		private String id;
		private String name;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
	}
	
}
