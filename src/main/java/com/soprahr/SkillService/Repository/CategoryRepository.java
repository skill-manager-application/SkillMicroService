package com.soprahr.SkillService.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.soprahr.SkillService.entites.CategoryEntity;

@Repository
public interface CategoryRepository extends MongoRepository<CategoryEntity, String>{

}


