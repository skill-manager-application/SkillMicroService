package com.soprahr.SkillService.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.soprahr.SkillService.entites.SkillEntity;

@Repository
public interface SkillRepository extends MongoRepository<SkillEntity, String> {
}

