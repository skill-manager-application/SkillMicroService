package com.soprahr.SkillService.Repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.soprahr.SkillService.entites.UserInfo;


@Repository
public interface userInfoRepository  extends MongoRepository<UserInfo, String>{
	
	@Query("{userId : ?0}") 
	UserInfo findUserInfoByUserId(String userId);
    List<UserInfo> findBySkillsSkillId(String skillId);

	
	
}
