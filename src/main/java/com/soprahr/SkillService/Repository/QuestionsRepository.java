package com.soprahr.SkillService.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.soprahr.SkillService.entites.QuestionsEntity;

@Repository
public interface QuestionsRepository extends MongoRepository<QuestionsEntity, String>{
}
