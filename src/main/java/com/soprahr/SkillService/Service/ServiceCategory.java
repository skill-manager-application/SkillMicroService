package com.soprahr.SkillService.Service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprahr.SkillService.Repository.CategoryRepository;
import com.soprahr.SkillService.entites.CategoryEntity;

@Service
public class ServiceCategory {
	@Autowired
	CategoryRepository categoryRepository;
	
	 public CategoryEntity createCategory(String name) {
	        LocalDateTime now = LocalDateTime.now();
	        CategoryEntity NewCategory = new CategoryEntity(name);
	        NewCategory.setCreatedAt(now);
	        NewCategory.setUpdatedAt(now);
	        return categoryRepository.save(NewCategory);
	    }
	    
	    
	    public List<CategoryEntity>getAllCategorys(){
	    	List<CategoryEntity> list = categoryRepository.findAll();
	    	return list ;
	    }
	    
	    public void deleteCategory(String id) {
	    	categoryRepository.deleteById(id);
	    }
}
