package com.soprahr.SkillService.Service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprahr.SkillService.Repository.SkillRepository;
import com.soprahr.SkillService.Repository.userInfoRepository;
import com.soprahr.SkillService.entites.SkillEntity;
import com.soprahr.SkillService.entites.UserInfo;
import com.soprahr.SkillService.entites.UserInfo.skills;

@Service
public class ServiceUserInfo {
	@Autowired
	userInfoRepository userInfoRepository;
	
	@Autowired
	SkillRepository skillrepository;
	
	 public UserInfo createUserInfo(UserInfo userInfo) {
	        return userInfoRepository.save(userInfo);
	    }
	    
	    
	    public List<UserInfo>getAllUserInfos(){
	    	List<UserInfo> list = userInfoRepository.findAll();
	    	return list ;
	    }
	    
	    public void deleteUserInfo(String id) {
	    	userInfoRepository.deleteById(id);
	    }
	    
	    public UserInfo addSkillToUser(String skillId,String userId) {
	    	UserInfo user = userInfoRepository.findUserInfoByUserId(userId);
	    	SkillEntity skill = skillrepository.findById(skillId).get();
	    	skills newAssSkill = new skills();
	    	LocalDateTime now = LocalDateTime.now();
	    	newAssSkill.setAssigneddAt(now);
	    	newAssSkill.setSkillId(skillId);
	    	newAssSkill.setSkillName(skill.getName());
	    	newAssSkill.setTestToken(false);
	    	user.addSkills(newAssSkill);
	    	userInfoRepository.save(user);
	    	return user;
	    }
}
