package com.soprahr.SkillService.Service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprahr.SkillService.Repository.CategoryRepository;
import com.soprahr.SkillService.Repository.SkillRepository;
import com.soprahr.SkillService.Request.SkillRequest;
import com.soprahr.SkillService.entites.CategoryEntity;
import com.soprahr.SkillService.entites.SkillEntity;

@Service
public class ServiceSkills {

	@Autowired
	SkillRepository skillRepository;

	@Autowired
	CategoryRepository categoryRepository;

	public SkillEntity createSkill(SkillRequest skill, String categoryId) {
		CategoryEntity category = categoryRepository.findById(categoryId).orElseThrow(() ->
        new IllegalArgumentException("Category with ID " + categoryId + " not found"));
		//INIT and Save the Skill in database
		SkillEntity newSkillToSave = new SkillEntity(skill.getName(),skill.getDescription() ,category, skill.getLevel(), skill.getKeywords(), skill.getRelatedSkills(), skill.getResources(), null, null);
        LocalDateTime now = LocalDateTime.now();
        newSkillToSave.setCreatedAt(now);
        newSkillToSave.setUpdatedAt(now);
        SkillEntity newSkill = skillRepository.save(newSkillToSave);
		
        // update the corresponding category with the new skill
        category.addSkill(newSkill);
        categoryRepository.save(category);
        
		return newSkill;
	}

	public List<SkillEntity> getAllSkills() {
		return skillRepository.findAll();
	}

	public void deleteSkill(String id) {
		skillRepository.deleteById(id);
	}
	public SkillEntity getSkillById(String id) {
		return skillRepository.findById(id).isPresent() ?skillRepository.findById(id).get() : null;
	}
}
