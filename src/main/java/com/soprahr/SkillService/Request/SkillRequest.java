package com.soprahr.SkillService.Request;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



public class SkillRequest {
		
		@NotBlank(message = "Ce Champs Ne doit etre null !")
		@NotEmpty(message = "Ce Champs Ne doit etre null !")
		@NotNull(message = "Ce Champs Ne doit etre null !")
		@Size(min = 3,message = "Ce champs doit avoir au moins 3 Caractere !")
	    private String name;

	    private String description;
	    
	    private float level;

	    private List<String> keywords;

	    private List<String> relatedSkills;

	    private List<String> resources;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public float getLevel() {
			return level;
		}

		public void setLevel(float level) {
			this.level = level;
		}

		public List<String> getKeywords() {
			return keywords;
		}

		public void setKeywords(List<String> keywords) {
			this.keywords = keywords;
		}

		public List<String> getRelatedSkills() {
			return relatedSkills;
		}

		public void setRelatedSkills(List<String> relatedSkills) {
			this.relatedSkills = relatedSkills;
		}

		public List<String> getResources() {
			return resources;
		}

		public void setResources(List<String> resources) {
			this.resources = resources;
		}
}
